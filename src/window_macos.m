#import <AppKit/AppKit.h>

void macos_force_foreground_level() {
  [NSApp activateIgnoringOtherApps: YES];

#ifdef MACOS_DARK_TITLEBAR
  // Set dark titlebar
  [[NSApplication sharedApplication].windows[0] setAppearance:[NSAppearance appearanceNamed:NSAppearanceNameVibrantDark]];
#endif
}
